# Pipeline Components: _Template_

![Project Stage][project-stage-shield]
![Project Maintenance][maintenance-shield]
[![License][license-shield]](LICENSE)
[![GitLab CI][gitlabci-shield]][gitlabci]
[![Docker Pulls][pulls-shield]][dockerhub]

## Usage

The image is for running _template_, _template_ is installed in /app/ in case you need to customize the install before usage.

## Examples

```yaml
_template_:
  stage: linting
  image: registry.gitlab.com/pipeline-components/_template_:latest
  script:
    - _template_ .
```

## Versioning

This project uses [Semantic Versioning][semver] for its version numbering.

## Support

Got questions?

You have several options to get them answered:
Check the [discord channel][discord]
You could also [open an issue here][issue]

## Contributing

This is an active open-source project. We are always open to people who want to
use the code or contribute to it.

We've set up a separate document for our [contribution guidelines](CONTRIBUTING.md).

Thank you for being involved! :heart_eyes:

## Authors & contributors

The original setup of this repository is by [Robbert Müller][mjrider].

For a full list of all authors and contributors,
check [the contributor's page][contributors].

## License

Created by [Robbert Müller][mjrider] licensed under a [MIT License][license-link] (MIT).

[commits]: https://gitlab.com/pipeline-components/_template_/-/commits/main
[contributors]: https://gitlab.com/pipeline-components/_template_/-/graphs/main
[discord]: https://discord.gg/vhxWFfP
[dockerhub]: https://hub.docker.com/r/pipelinecomponents/_template_
[gitlabci-shield]: https://img.shields.io/gitlab/pipeline/pipeline-components/_template_.svg
[gitlabci]: https://gitlab.com/pipeline-components/_template_/-/commits/main
[issue]: https://gitlab.com/pipeline-components/_template_/issues
[license-link]: ./LICENSE
[license-shield]: https://img.shields.io/badge/License-MIT-green.svg
[maintenance-shield]: https://img.shields.io/maintenance/yes/2025.svg
[mjrider]: https://gitlab.com/mjrider
[project-stage-shield]: https://img.shields.io/badge/project%20stage-production%20ready-brightgreen.svg
[pulls-shield]: https://img.shields.io/docker/pulls/pipelinecomponents/_template_.svg
[releases]: https://gitlab.com/pipeline-components/_template_/tags
[repository]: https://gitlab.com/pipeline-components/_template_
[semver]: http://semver.org/spec/v2.0.0.html
