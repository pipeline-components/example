# ==============================================================================
# Add https://gitlab.com/pipeline-components/org/base-entrypoint
# ------------------------------------------------------------------------------
FROM pipelinecomponents/base-entrypoint:0.5.1 as entrypoint

# ==============================================================================
# Component specific
# ------------------------------------------------------------------------------
FROM alpine:3.21.3
COPY app /app/
WORKDIR /app/
RUN echo "➡ Magic commands to make _template_ work go here ⬅"

# ==============================================================================
# Generic for all components
# ------------------------------------------------------------------------------
COPY --from=entrypoint /entrypoint.sh /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
ENV DEFAULTCMD _template_

WORKDIR /code/

# ==============================================================================
# Container meta information
# ------------------------------------------------------------------------------
ARG BUILD_DATE
ARG BUILD_REF

LABEL \
    maintainer="Robbert Müller <spam.me@grols.ch>" \
    org.label-schema.build-date=${BUILD_DATE} \
    org.label-schema.description="_Template_ in a container for gitlab-ci" \
    org.label-schema.name="_Template_" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.url="https://pipeline-components.dev/" \
    org.label-schema.usage="https://gitlab.com/pipeline-components/_template_/blob/main/README.md" \
    org.label-schema.vcs-ref=${BUILD_REF} \
    org.label-schema.vcs-url="https://gitlab.com/pipeline-components/_template_/" \
    org.label-schema.vendor="Pipeline Components" \
    org.opencontainers.image.authors="Robbert Müller <spam.me@grols.ch>" \
    org.opencontainers.image.created=${BUILD_DATE} \
    org.opencontainers.image.description="${BUILD_DESCRIPTION}" \
    org.opencontainers.image.documentation="https://gitlab.com/pipeline-components/_template_/blob/main/README.md" \
    org.opencontainers.image.licenses="MIT" \
    org.opencontainers.image.revision=${BUILD_REF} \
    org.opencontainers.image.source="https://gitlab.com/pipeline-components/_template_/" \
    org.opencontainers.image.title="_Template_" \
    org.opencontainers.image.url="https://pipeline-components.dev/" \
    org.opencontainers.image.vendor="Pipeline Components" \
    org.opencontainers.image.version=${BUILD_VERSION}
